#!/bin/bash
###########################################
#    _____            _         
#   |  __ \          | |        
#   | |__) |___ _ __ | |_ _   _ 
#   |  _  // _ \ '_ \| __| | | |
#   | | \ \  __/ | | | |_| |_| |
#   |_|  \_\___|_| |_|\__|\__, |
#                          __/ |
#                         |___/ 
#
###########################################
###########################################
# Variables
TIMESTAMP=$(date  +'%Y-%m-%d_%H-%M-%S')
ANDROIDPACKAGENAME=io.renty.renty
DEFAULTEMULATORID=emulator-5554
DEFAULTUSERNAME=user_michel
DEFAULTPASSWORD=michel
OUTDIR="$(pwd)/screenshots"
EMUDIR=/sdcard/
FLOATINGBUTTONTIMEOUT=4
APICALLTIMEOUT=45
INPUTTIMEOUT=2

set -e # Exit on first error

## Checking if some variable are up to date
if [ -v TEST_USERNAME ]; then DEFAULTUSERNAME=$TEST_USERNAME; fi
if [ -v TEST_PASSWORD ]; then DEFAULTPASSWORD=$TEST_PASSWORD; fi
###
## Starting tests
adb -s $DEFAULTEMULATORID shell screencap -p $EMUDIR/welcome_screen_$TIMESTAMP.png
echo "Hitting login.name field"
adb shell input tap 157 802 && sleep $INPUTTIMEOUT
echo "Entering username \"$DEFAULTUSERNAME\""
adb shell input text $DEFAULTUSERNAME
echo "Getting login.password field"
adb shell input tap 131 630 && sleep $INPUTTIMEOUT
echo "Entering password \"$DEFAULTPASSWORD\""
adb shell input text $DEFAULTPASSWORD && sleep $INPUTTIMEOUT
echo "Taking screenshot of filled login form"
adb -s $DEFAULTEMULATORID shell screencap -p $EMUDIR/filled_login_form_$TIMESTAMP.png
echo "Hitting submit button and wait for api response"
adb shell input tap 528 835 && sleep $APICALLTIMEOUT
echo "Taking screenshot of login submit result (should be dashboard)"
adb -s $DEFAULTEMULATORID shell screencap -p $EMUDIR/login_result_$TIMESTAMP.png
echo "Hitting floating action button" 
adb -s $DEFAULTEMULATORID shell input tap 948 1647 && sleep $FLOATINGBUTTONTIMEOUT
echo "Taking screenshot of floating button click"
adb -s $DEFAULTEMULATORID shell screencap -p $EMUDIR/floating_actionbutton_$TIMESTAMP.png
echo "Hitting logout"
adb shell input tap 946 1284 && sleep $INPUTTIMEOUT
echo "Taking screenshots after logout (should be menu screen)"
adb -s $DEFAULTEMULATORID shell screencap -p $EMUDIR/after_logout_$TIMESTAMP.png
echo "Pulling screenshots from emulator ..."
adb pull /sdcard/welcome_screen_$TIMESTAMP.png $OUTDIR
adb pull /sdcard/filled_login_form_$TIMESTAMP.png $OUTDIR
adb pull /sdcard/login_result_$TIMESTAMP.png $OUTDIR
adb pull /sdcard/floating_actionbutton_$TIMESTAMP.png $OUTDIR
adb pull /sdcard/after_logout_$TIMESTAMP.png $OUTDIR
adb -s $DEFAULTEMULATORID emu kill &>/dev/null
echo "Done ! See results in screenshots/ directory !"
 