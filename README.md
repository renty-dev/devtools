# Devtool -  Renty

## A. Sonarqube - Code Quality Scanner

Build :

```docker build -t scanner sonarqube```

Scan :

1. Run sonarqube server and gui
```docker run -d --name sonarqube -p 9000:9000 -p 9092:9092 sonarqube``` 

2. Moove to the directory you want to scan and then
``` docker run -ti --network=host -v $(pwd):/usr/src -e SONAR_HOST_URL=http://locahost:9000 scanner```

## B. Android-screenshots - Aceptance Testing for Renty Android App

Build :

```docker build -t android-screenshots android-screenshots```

Run :

## C. Gitlab-Runner - Custom gitlab runner config


## D. Heroku-Grafana - Ready to user Grafana Monitoring Dashboard 

