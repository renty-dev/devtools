#!/usr/bin/env bash
# Adding an issue report on the end thanks to https://github.com/lequal/sonar-cnes-report/releases
cd /usr/src && sonar-scanner && \
    sleep 30 && java -jar /usr/lib/sonar-scanner/cnes-report.jar \
    -p renty_golang -o ./reports